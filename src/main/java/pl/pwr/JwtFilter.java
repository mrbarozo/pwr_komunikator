package pl.pwr;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import pl.pwr.communicator.commons.AuthConstants;
import pl.pwr.communicator.repository.UserRepository;
import pl.pwr.communicator.service.EncryptionService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class JwtFilter implements javax.servlet.Filter {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EncryptionService encryptionService;

    @SneakyThrows
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

        String authorizationHeader = httpServletRequest.getHeader("Authorization");

        if(authorizationHeader == null || authorizationHeader.isEmpty())
            throw new ServletException("Missing Authorization header");

        if(encryptionService == null) {
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletRequest.getServletContext());
            encryptionService = webApplicationContext.getBean(EncryptionService.class);
        }

        String tokenRaw = encryptionService.decryptMessage(AuthConstants.authRsaKeyPair.getRsaPrivateKeySpec(), authorizationHeader);

        if (tokenRaw == null || !tokenRaw.startsWith("Bearer ")) {
            throw new ServletException("Invalid Authorization header");
        } else {
            String token = tokenRaw.substring(7);
            Claims claims = Jwts.parser().setSigningKey(AuthConstants.SIGNING_KEY).parseClaimsJws(token).getBody();
            servletRequest.setAttribute("claims", claims);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
