package pl.pwr.communicator.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("*")
                .exposedHeaders("Access-Control-Allow-Headers", "Access-Control-Allow-Origin", "Content-Disposition")
                .allowedMethods("GET", "PUT", "POST", "PATCH", "DELETE", "OPTIONS");

    }
}

