package pl.pwr.communicator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.pwr.communicator.commons.RSAPrivateKeySpecHelper;
import pl.pwr.communicator.commons.RSAPublicKeySpecHelper;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class User {

    @Id
    private String id;

    private String login;

    private String passwordHash;

    private RSAPublicKeySpecHelper userPublicKeySpec;
    private RSAPublicKeySpecHelper serverPublicKeySpec;
    private RSAPrivateKeySpecHelper serverPrivateKeySpec;

    private Boolean active;
}