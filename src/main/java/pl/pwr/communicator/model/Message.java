package pl.pwr.communicator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import pl.pwr.communicator.commons.MessageStatus;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Message {

    @Id
    private String id;

    private String senderLogin;

    private String receiverLogin;

    private String encodedMessage;

    private Long sendTime;

    private MessageStatus status;
}
