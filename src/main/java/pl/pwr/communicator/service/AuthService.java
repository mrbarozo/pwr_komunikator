package pl.pwr.communicator.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pl.pwr.communicator.commons.*;
import pl.pwr.communicator.dto.SuccessfulLoginDto;
import pl.pwr.communicator.dto.UserDto;
import pl.pwr.communicator.dto.UserLoginDto;
import pl.pwr.communicator.dto.UserRegistrationDto;
import pl.pwr.communicator.mapper.UserMapper;
import pl.pwr.communicator.model.User;
import pl.pwr.communicator.repository.UserRepository;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthService {
    private static final long jwtLifeInMilliseconds = 3600000;

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final PasswordEncoderService encoderService;

    @Autowired
    private final EncryptionService encryptionService;

    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    public String getAllUsers(TokenClaims tokenClaims) throws Exception {
        List<User> all = userRepository.findAll();
        List<UserDto> userDtoList = all.stream().map(userMapper::userToUserDto).collect(Collectors.toList());
        return encryptionService.encryptUserDtoList(tokenClaims.getSub(), userDtoList);
    }

    public ResponseEntity<String> getServerAuthEncryptionKey() {
        return new ResponseEntity<>(encryptionService.getPublicKeyAsString(AuthConstants.authRsaKeyPair.getRsaPublicKeySpec()), HttpStatus.OK);
    }

    public ResponseEntity<String> userRegister(String encryptedCredentials) {
        UserRegistrationDto userRegistrationDto;
        try {
            userRegistrationDto = encryptionService.decryptUserRegistrationCredentials(encryptedCredentials);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>("Request body is malformed!", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("Server error, please report", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (userExists(userRegistrationDto))
            return new ResponseEntity<>("User with this login already exists", HttpStatus.CONFLICT);
        createNewUser(userRegistrationDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean userExists(UserRegistrationDto userRegistrationDto) {
        return userRepository.findByLogin(userRegistrationDto.getLogin()).isPresent();
    }

    private void createNewUser(UserRegistrationDto userRegistrationDto) {
        User newUser = new User();
        newUser.setLogin(userRegistrationDto.getLogin());
        newUser.setPasswordHash(encoderService.getPasswordHash(userRegistrationDto.getPassword()));
        newUser.setUserPublicKeySpec(userRegistrationDto.getRsaPublicKey());

        RSAKeyPairSpec rsaKeyPairSpec = RSAKeyPairSpec.generateKeyPairSpec();
        newUser.setServerPrivateKeySpec(new RSAPrivateKeySpecHelper(rsaKeyPairSpec.getRsaPrivateKeySpec()));
        newUser.setServerPublicKeySpec(new RSAPublicKeySpecHelper(rsaKeyPairSpec.getRsaPublicKeySpec()));
        userRepository.save(newUser);
    }

    public ResponseEntity<String> userLogin(String encryptedCredentials) {
        UserLoginDto userLoginDto;
        try {
            userLoginDto = encryptionService.decryptUserCredentials(encryptedCredentials);
        } catch (Exception e) {
            return new ResponseEntity<>("Request body is malformed", HttpStatus.BAD_REQUEST);
        }

        if (!areUserCredentialsValid(userLoginDto))
            return new ResponseEntity<>("Provided credentials don't fit to any user", HttpStatus.NOT_FOUND);
        String encryptedSuccessfulLoginDTO;
        try {
            SuccessfulLoginDto successfulLoginDTO = new SuccessfulLoginDto(issueToken(userLoginDto), getUserServerEncryptionKey(userLoginDto.getLogin()));
            encryptedSuccessfulLoginDTO = encryptionService.encryptSuccessfulLoginDTO(getUserEncryptionKey(userLoginDto.getLogin()).getRSAPublicKeySpec(), successfulLoginDTO);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(encryptedSuccessfulLoginDTO, HttpStatus.OK);
    }

    private boolean areUserCredentialsValid(UserLoginDto userLoginDto) {
        return userRepository.findByLogin(userLoginDto.getLogin())
                .map(user -> encoderService.isPasswordValid(userLoginDto.getPassword(), user.getPasswordHash()))
                .orElse(false);
    }

    private String issueToken(UserLoginDto userLoginDto) {
        long currentTimeMillis = System.currentTimeMillis();
        return Jwts.builder()
                .setSubject(userLoginDto.getLogin())
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + jwtLifeInMilliseconds))
                .signWith(SignatureAlgorithm.HS512, AuthConstants.SIGNING_KEY)
                .compact();
    }

    private RSAPublicKeySpecHelper getUserServerEncryptionKey(String userLogin) throws Exception {
        return userRepository.findByLogin(userLogin)
                .map(User::getServerPublicKeySpec)
                .orElseThrow(() -> new Exception("Can't find user"));
    }

    private RSAPublicKeySpecHelper getUserEncryptionKey(String userLogin) throws Exception {
        return userRepository.findByLogin(userLogin)
                .map(User::getUserPublicKeySpec)
                .orElseThrow(() -> new Exception("Can't find user"));
    }
}
