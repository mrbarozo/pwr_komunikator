package pl.pwr.communicator.service.customrsa.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.math.BigInteger;

@Data
@Builder
@Getter
@AllArgsConstructor
public class RSAPublicKey {
    private final BigInteger e;
    private final BigInteger n;
}