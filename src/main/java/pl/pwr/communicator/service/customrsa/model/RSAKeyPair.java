package pl.pwr.communicator.service.customrsa.model;

import lombok.Getter;

import java.math.BigInteger;

@Getter
public class RSAKeyPair {
    private final RSAPrivateKey privateKey;
    private final RSAPublicKey publicKey;

    public RSAKeyPair(BigInteger d, BigInteger e, BigInteger n) {
        this.privateKey = new RSAPrivateKey(d, n);
        this.publicKey = new RSAPublicKey(e, n);
    }
}
