package pl.pwr.communicator.service.customrsa;

import pl.pwr.communicator.service.customrsa.model.RSAKeyPair;

import java.math.BigInteger;
import java.util.Random;

public class RSAKeyPairGenerator {
    private RSAKeyPairGenerator() {

    }

    public static RSAKeyPair generateKeyPair(int bitLength) {
        BigInteger bigNumberPrivateExponent = BigInteger.ZERO;
        BigInteger bigNumberModulo = BigInteger.ZERO;
        BigInteger bigNumberPublicExponent = BigInteger.ZERO;;
        while(bigNumberPrivateExponent.equals(BigInteger.ZERO)) {
            BigInteger bigNumberP = BigInteger.probablePrime(bitLength, new Random());
            BigInteger bigNumberQ = BigInteger.probablePrime(bitLength, new Random());

            BigInteger bigNumberPhi = bigNumberP.subtract(BigInteger.ONE).multiply(bigNumberQ.subtract(BigInteger.ONE));
            bigNumberModulo = bigNumberP.multiply(bigNumberQ);

            bigNumberP = null;
            bigNumberQ = null;

            bigNumberPublicExponent = generateRelativePrimeNumber(bigNumberPhi);
            bigNumberPrivateExponent = generateModularMultiplicativeInverse(bigNumberPublicExponent, bigNumberPhi);
        }
        return new RSAKeyPair(bigNumberPrivateExponent, bigNumberPublicExponent, bigNumberModulo);
    }

    private static BigInteger generateRelativePrimeNumber(BigInteger number) {
        BigInteger e = new BigInteger("3");
        BigInteger bigIntegerTwo = new BigInteger("2");
        for(; !e.gcd(number).equals(BigInteger.ONE); e = e.add(bigIntegerTwo));
        return e;
    }

    private static BigInteger generateModularMultiplicativeInverse(BigInteger bigNumberPublicExponent, BigInteger bigNumberPhi) {
        BigInteger u, w, x, z, q;

        u = BigInteger.ONE; w = bigNumberPublicExponent;
        x = BigInteger.ZERO; z = bigNumberPhi;
        while( w.compareTo(BigInteger.ZERO) > 0 ) {
            if( w.compareTo(z) < 0 ) {
                q = u; u = x; x = q;
                q = w; w = z; z = q;
            }
            q = w.divide(z);
            u = u.subtract(q.multiply(x));
            w = w.subtract(q.multiply(z));
        }
        if( z.equals(BigInteger.ONE) )
            if( x.compareTo(BigInteger.ZERO) < 0 )
                return x.add(bigNumberPhi);
        return BigInteger.ZERO;
    }

}
