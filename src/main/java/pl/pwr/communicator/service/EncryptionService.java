package pl.pwr.communicator.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pwr.communicator.commons.AuthConstants;
import pl.pwr.communicator.commons.RSAPublicKeySpecHelper;
import pl.pwr.communicator.dto.*;
import pl.pwr.communicator.repository.UserRepository;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

@Service
public class EncryptionService {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static KeyFactory keyFactory;

    private final UserRepository userRepository;

    @Autowired
    public EncryptionService(UserRepository userRepository) throws NoSuchAlgorithmException {
        keyFactory = KeyFactory.getInstance("RSA");
        this.userRepository = userRepository;
    }

    public UserLoginDto decryptUserCredentials(String encryptedCredentialsBase64) throws Exception {
        String message = decryptMessage(AuthConstants.authRsaKeyPair.getRsaPrivateKeySpec(), encryptedCredentialsBase64);
        return objectMapper.readValue(message, UserLoginDto.class);
    }

    public UserRegistrationDto decryptUserRegistrationCredentials(String encryptedRegistrationCredentialsBase64) throws Exception {
        String message = decryptMessage(AuthConstants.authRsaKeyPair.getRsaPrivateKeySpec(), encryptedRegistrationCredentialsBase64);
        return objectMapper.readValue(message, UserRegistrationDto.class);
    }

    public RSAPublicKey decryptUserPublicKey(String userPublicKeyBase64) throws Exception {
        String message = decryptMessage(AuthConstants.authRsaKeyPair.getRsaPrivateKeySpec(), userPublicKeyBase64);
        return objectMapper.readValue(message, RSAPublicKey.class);
    }

    public String encryptPublicKey(RSAPublicKeySpec encryptionRsaPublicKeySpec, RSAPublicKeySpec rsaPublicKeyToSpecSend) throws Exception {
        String publicKeyJson = getPublicKeyAsString(rsaPublicKeyToSpecSend);
        return encryptMessage(encryptionRsaPublicKeySpec, publicKeyJson);
    }

    public String encryptSuccessfulLoginDTO(RSAPublicKeySpec rsaPublicKeySpec, SuccessfulLoginDto successfulLoginDto) throws Exception {
        String successfulLoginDTOJson = objectMapper.writeValueAsString(successfulLoginDto);
        return encryptMessage(rsaPublicKeySpec, successfulLoginDTOJson);
    }

    public String encryptUserDtoList(String userLogin, List<UserDto> userDtoList) throws Exception {
        String userDtoListJson = objectMapper.writeValueAsString(userDtoList);
        return encryptMessage(getUserPublicKey(userLogin), userDtoListJson);
    }

    public String getPublicKeyAsString(RSAPublicKeySpec rsaPublicKeySpec) {
        RSAPublicKeySpecHelper rsaPublicKeySpecHelper = new RSAPublicKeySpecHelper(rsaPublicKeySpec);
        return "{\"modulus\":" + rsaPublicKeySpecHelper.getModulus() + ",\"publicExponent\":" + rsaPublicKeySpecHelper.getPublicExponent() + "}";
    }

    private RSAPublicKeySpec getUserPublicKey(String userLogin) throws Exception {
        return userRepository.findByLogin(userLogin)
                .map(user -> user.getUserPublicKeySpec().getRSAPublicKeySpec())
                .orElseThrow(() -> new Exception(""));
    }

    public String encryptMessage(RSAPublicKeySpec rsaPublicKeySpec, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, getRsaPublicKeyFromSpec(rsaPublicKeySpec));
        byte[] encryptedMessage = new byte[0];
        String[] messageBlocks = message.split("(?<=\\G.{117})");
        for (String messageBlock : messageBlocks) {
            byte[] encryptedBlock = encryptBlock(cipher, messageBlock);
            encryptedMessage = joinBlocks(encryptedMessage, encryptedBlock);
        }
        return new String(Base64.getEncoder().encode(encryptedMessage));
    }

    private byte[] encryptBlock(Cipher cipher, String messageBlock) throws BadPaddingException, IllegalBlockSizeException {
        cipher.update(messageBlock.getBytes());
        return cipher.doFinal();
    }

    private byte[] joinBlocks(byte[] block1, byte[] block2) {
        byte[] joinedBlocks = new byte[block1.length + block2.length];
        System.arraycopy(block1,0, joinedBlocks,0, block1.length);
        System.arraycopy(block2,0, joinedBlocks, block1.length, block2.length);
        return joinedBlocks;
    }

    public String decryptMessage(RSAPrivateKeySpec rsaPrivateKeySpec, String encodedMessage) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, getRsaPrivateKeyFromSpec(rsaPrivateKeySpec));
        byte[] rawEncodedMessage = Base64.getDecoder().decode(encodedMessage);
        StringBuilder decodedMessageBuilder = new StringBuilder("");
        for(int blockStart = 0; blockStart < rawEncodedMessage.length; blockStart += AuthConstants.RSA_MAX_BLOCK_BYTE_SIZE) {
            byte[] block =  Arrays.copyOfRange(rawEncodedMessage, blockStart, blockStart + AuthConstants.RSA_MAX_BLOCK_BYTE_SIZE);
            decodedMessageBuilder.append(decryptBlock(cipher, block));
        }
        return decodedMessageBuilder.toString();
    }

    private String decryptBlock(Cipher cipher, byte[] encodedBlock) throws BadPaddingException, IllegalBlockSizeException {
        cipher.update(encodedBlock);
        return new String(cipher.doFinal());
    }

    private RSAPublicKey getRsaPublicKeyFromSpec(RSAPublicKeySpec rsaPublicKeySpec) throws InvalidKeySpecException {
        return (RSAPublicKey) keyFactory.generatePublic(rsaPublicKeySpec);
    }

    private RSAPrivateKey getRsaPrivateKeyFromSpec(RSAPrivateKeySpec rsaPrivateKeySpec) throws InvalidKeySpecException {
        return (RSAPrivateKey) keyFactory.generatePrivate(rsaPrivateKeySpec);
    }
}
