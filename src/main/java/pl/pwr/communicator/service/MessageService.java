package pl.pwr.communicator.service;

import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;
import pl.pwr.communicator.commons.MessageStatus;
import pl.pwr.communicator.dto.MessageDto;
import pl.pwr.communicator.mapper.MessageMapper;
import pl.pwr.communicator.model.Message;
import pl.pwr.communicator.model.User;
import pl.pwr.communicator.repository.MessageRepository;
import pl.pwr.communicator.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageService {

    private MessageRepository messageRepository;

    private UserRepository userRepository;

    private MessageMapper messageMapper = Mappers.getMapper(MessageMapper.class);

    public List<MessageDto> getAllNewMessages(String login) {

        Optional<User> user = userRepository.findByLogin(login);

        if (!user.isPresent()) return null;

        List<Message> messages = messageRepository.findAllByReceiverLoginAndStatus(user.get().getLogin(), MessageStatus.WAITING);

        for (Message message : messages) {
            message.setStatus(MessageStatus.RECEIVED);
        }

        return messages.stream()
                .map(messageMapper::messageToMessageDto)
                .collect(Collectors.toList());
    }

    public List<MessageDto> getAllUserMessages(String login) {
        Optional<User> user = userRepository.findByLogin(login);
        if (!user.isPresent()) return null;
        
        List<Message> messages = messageRepository.findAllByReceiverLogin(user.get().getLogin());

        for (Message message : messages) {
            message.setStatus(MessageStatus.RECEIVED);
        }

        return messages.stream()
                .map(messageMapper::messageToMessageDto)
                .collect(Collectors.toList());
    }

    public void sendMessage(MessageDto messageDto) {
        Message message = messageMapper.messageDtoToMessage(messageDto);
        message.setStatus(MessageStatus.WAITING);
        message.setSendTime(System.currentTimeMillis());
        messageRepository.save(message);
    }

}