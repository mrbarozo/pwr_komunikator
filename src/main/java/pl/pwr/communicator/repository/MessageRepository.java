package pl.pwr.communicator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.pwr.communicator.commons.MessageStatus;
import pl.pwr.communicator.model.Message;

import java.util.List;

@Repository
public interface MessageRepository extends MongoRepository<Message, String> {
    List<Message> findAllByReceiverLogin(String receiverLogin);

    List<Message> findAllBySenderLogin(String senderLogin);

    List<Message> findAllByReceiverLoginAndStatus(String receiverLogin, MessageStatus messageStatus);
}
