package pl.pwr.communicator.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MessageDto {

    private String senderLogin;

    private String receiverLogin;

    private String encodedMessage;

    private Long sendTime;

}