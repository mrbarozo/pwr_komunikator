package pl.pwr.communicator.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.pwr.communicator.commons.RSAPublicKeySpecHelper;

@Data
@AllArgsConstructor
public class SuccessfulLoginDto {
    private String token;
    private RSAPublicKeySpecHelper serverPublicKey;
}
