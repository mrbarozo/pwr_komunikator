package pl.pwr.communicator.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pwr.communicator.commons.RSAPublicKeySpecHelper;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserDto {
    private String login;
    private RSAPublicKeySpecHelper userPublicKeySpec;
    private Boolean active;
}

