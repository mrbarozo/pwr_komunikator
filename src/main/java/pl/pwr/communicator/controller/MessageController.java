package pl.pwr.communicator.controller;

import io.swagger.v3.oas.annotations.headers.Header;
import jdk.nashorn.internal.parser.Token;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pwr.communicator.commons.AuthConstants;
import pl.pwr.communicator.commons.TokenUtilities;
import pl.pwr.communicator.dto.MessageDto;
import pl.pwr.communicator.dto.UserDto;
import pl.pwr.communicator.service.AuthService;
import pl.pwr.communicator.service.EncryptionService;
import pl.pwr.communicator.service.MessageService;

import java.util.List;

@RestController
@RequestMapping("/message")
@RequiredArgsConstructor
public class MessageController {

    private final AuthService authService;

    private final MessageService messageService;

    private final EncryptionService encryptionService;

    @GetMapping(value = "/users")
    public String getAllUsers(@RequestHeader("Authorization") String token) throws Exception {
        String rawToken = encryptionService.decryptMessage(AuthConstants.authRsaKeyPair.getRsaPrivateKeySpec(), token);
        return authService.getAllUsers(TokenUtilities.getTokenClaims(rawToken));
    }

    @GetMapping(value = "/messages")
    public List<MessageDto> getMyMessages(@RequestParam String login) {
        return messageService.getAllUserMessages(login);
    }

    @GetMapping(value = "/newMessages")
    public List<MessageDto> getMyNewMessages(@RequestParam String login) {
        return messageService.getAllNewMessages(login);
    }

    @PostMapping(value = "/send")
    public ResponseEntity<String> sendMessage(@RequestBody MessageDto messageDto) {
        messageService.sendMessage(messageDto);
        return ResponseEntity.ok("Pomyślnie wysłano wiadomość");
    }
}
