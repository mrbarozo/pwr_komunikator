package pl.pwr.communicator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.pwr.communicator.commons.AuthConstants;
import pl.pwr.communicator.model.User;
import pl.pwr.communicator.repository.UserRepository;
import pl.pwr.communicator.service.AuthService;

import java.util.List;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    private final UserRepository userRepository;

    @Autowired
    public AuthController(AuthService authService, UserRepository userRepository) {
        this.authService = authService;
        this.userRepository = userRepository;
    }

    @GetMapping("/key")
    public ResponseEntity<String> getServerPublicKeyForAuthentication() {
        return authService.getServerAuthEncryptionKey();
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody String encryptedRegistrationCredentials) {
        return authService.userRegister(encryptedRegistrationCredentials);
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody String encryptedCredentials) {
        return authService.userLogin(encryptedCredentials);
    }

    @PostMapping(value = "/logout")
    @ResponseBody
    public ResponseEntity logout(@RequestParam String login) {

        //TODO token reset

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("token", "BYE!");
        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body("BYE!");
    }
}
