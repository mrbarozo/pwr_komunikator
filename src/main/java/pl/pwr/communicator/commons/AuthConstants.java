package pl.pwr.communicator.commons;

public class AuthConstants {
    public static final int RSA_KEY_BIT_LENGTH = 1024;
    public static final int RSA_MAX_BLOCK_BYTE_SIZE = RSA_KEY_BIT_LENGTH / 8;
    public static final String SIGNING_KEY = "&F)J@NcRfUjXn2r5u7x!A%D*G-KaPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq4t7w!";
    public static final RSAKeyPairSpec authRsaKeyPair = RSAKeyPairSpec.generateKeyPairSpec();
}
