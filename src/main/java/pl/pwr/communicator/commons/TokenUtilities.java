package pl.pwr.communicator.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Base64;

public final class TokenUtilities {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private TokenUtilities() {
    }

    public static TokenClaims getTokenClaims(String token) throws JsonProcessingException {
        return objectMapper.readValue(getJsonPayload(token), TokenClaims.class);
    }

    private static String getJsonPayload(String token) {
        if(token.split("\\.").length != 3)
            throw new IllegalArgumentException("Provided token is malformed");
        String base64EncodedPayload = token.split("\\.")[1];
        return new String(Base64.getDecoder().decode(base64EncodedPayload));
    }
}
