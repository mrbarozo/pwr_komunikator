package pl.pwr.communicator.commons;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MessageStatus {
    WAITING("Oczekująca na odebranie"),
    RECEIVED("Odebrana");

    private String message;
}
