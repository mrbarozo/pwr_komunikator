package pl.pwr.communicator.commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.security.spec.RSAPrivateKeySpec;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RSAPrivateKeySpecHelper {
    private BigInteger modulus;
    private BigInteger privateExponent;

    public RSAPrivateKeySpecHelper(RSAPrivateKeySpec rsaPublicKeySpec) {
        this.modulus = rsaPublicKeySpec.getModulus();
        this.privateExponent = rsaPublicKeySpec.getPrivateExponent();
    }
}
