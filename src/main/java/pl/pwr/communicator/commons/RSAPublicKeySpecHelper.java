package pl.pwr.communicator.commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.security.spec.RSAPublicKeySpec;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RSAPublicKeySpecHelper {
    BigInteger modulus;
    BigInteger publicExponent;

    public RSAPublicKeySpecHelper(RSAPublicKeySpec rsaPublicKeySpec) {
        this.modulus = rsaPublicKeySpec.getModulus();
        this.publicExponent = rsaPublicKeySpec.getPublicExponent();
    }

    public RSAPublicKeySpec getRSAPublicKeySpec() {
        return new RSAPublicKeySpec(getModulus(), getPublicExponent());
    }
}
