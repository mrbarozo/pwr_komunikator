package pl.pwr.communicator.commons;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

@Data
@AllArgsConstructor
public class RSAKeyPairSpec {
    private RSAPublicKeySpec rsaPublicKeySpec;
    private RSAPrivateKeySpec rsaPrivateKeySpec;

    public static RSAKeyPairSpec generateKeyPairSpec() {
        KeyPair keyPair = generateRsaKeyPair();

        RSAPublicKeySpec rsaPublicKeySpec = getRsaPublicKeySpec(keyPair.getPublic());
        RSAPrivateKeySpec rsaPrivateKeySpec = getRsaPrivateKeySpec(keyPair.getPrivate());

        return new RSAKeyPairSpec(rsaPublicKeySpec, rsaPrivateKeySpec);
    }

    private static KeyPair generateRsaKeyPair() {
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert keyPairGenerator != null;
        keyPairGenerator.initialize(AuthConstants.RSA_KEY_BIT_LENGTH);
        return keyPairGenerator.generateKeyPair();
    }

    private static RSAPublicKeySpec getRsaPublicKeySpec(PublicKey publicKey) {
        RSAPublicKey rsaPublicKey = (RSAPublicKey) publicKey;
        return new RSAPublicKeySpec(rsaPublicKey.getModulus(), rsaPublicKey.getPublicExponent());
    }

    private static RSAPrivateKeySpec getRsaPrivateKeySpec(PrivateKey privateKey) {
        RSAPrivateKey rsaPublicKey = (RSAPrivateKey) privateKey;
        return new RSAPrivateKeySpec(rsaPublicKey.getModulus(), rsaPublicKey.getPrivateExponent());
    }
}
