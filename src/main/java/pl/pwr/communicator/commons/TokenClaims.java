package pl.pwr.communicator.commons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenClaims {
    private String sub;
    private long iat;
    private long exp;
}
